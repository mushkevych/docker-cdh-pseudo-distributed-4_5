#!/bin/bash

set -x

# HBase section
service hbase-regionserver stop
service hbase-master stop
service zookeeper-server stop

# YARN section
service hadoop-mapreduce-historyserver stop
service hadoop-yarn-nodemanager stop
service hadoop-yarn-resourcemanager stop

# HDFS section
for x in `cd /etc/init.d ; ls hadoop-hdfs-*` ; do service $x stop ; done

$JAVA_HOME/bin/jps