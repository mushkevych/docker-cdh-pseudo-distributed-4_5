#!/bin/bash

# $FS_MOUNT_POINT stands for fully qualified Linux filesystem path 
# to the mount point where Hadoop folders and structures reside.
# For instance: /media/_media_1TB

FS_MOUNT_POINT=/dfs

mkdir --mode=777 $FS_MOUNT_POINT
chown -R hdfs:hadoop $FS_MOUNT_POINT

mkdir --mode=777 $FS_MOUNT_POINT/hadoop_tmp
chown -R hdfs:hadoop $FS_MOUNT_POINT/hadoop_tmp

mkdir $FS_MOUNT_POINT/hadoop_tmp/yarn
chown yarn:yarn -R $FS_MOUNT_POINT/hadoop_tmp/yarn
chmod 766 -R $FS_MOUNT_POINT/hadoop_tmp/yarn

mkdir --mode=777 $FS_MOUNT_POINT/zookeeper
chown -R zookeeper:zookeeper $FS_MOUNT_POINT/zookeeper


# initializes Zookeeper
service zookeeper-server init

# formats HDFS namenode
su - hdfs -c "hdfs namenode -format"
